package id.vendi.model.response;

public class BaseResponse {
  public int status;
  public String message;

  public BaseResponse(int status, String message) {
    this.status = status;
    this.message = message;
  }
}
