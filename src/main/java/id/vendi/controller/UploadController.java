package id.vendi.controller;

import id.vendi.model.UploadFile;
import id.vendi.model.response.BaseResponse;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/home")
public class UploadController {

  private static final Logger log = LoggerFactory.getLogger(UploadController.class);

  @Inject
  @Channel("ifg")
  Emitter<UploadFile> recordEmitter;

  private final String UPLOADED_FILE_PATH = "uploadfile/";

  @POST
  @Path("/upload")
  @Consumes("multipart/form-data")
  @Produces(MediaType.APPLICATION_JSON)
  public Response uploadFile(MultipartFormDataInput input) {

    String fileName;

    Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
    List<InputPart> inputParts = uploadForm.get("file");

    for (InputPart inputPart : inputParts) {

      try {

        MultivaluedMap<String, String> header = inputPart.getHeaders();
        fileName = getFileName(header);

        InputStream inputStream = inputPart.getBody(InputStream.class, null);

        byte[] bytes = IOUtils.toByteArray(inputStream);

        fileName = UPLOADED_FILE_PATH + fileName;

        String ext = FilenameUtils.getExtension(fileName);

        if (ext.equalsIgnoreCase("csv") || ext.equalsIgnoreCase("xls") || ext.equalsIgnoreCase("xlsx")) {
          String fileByte = Base64.getEncoder().encodeToString(bytes);

          recordEmitter.send(new UploadFile(fileName, fileByte));

          UploadController.log.info("Success send to consumer");
        } else {
          return Response.status(400).entity(new BaseResponse(400, "File must csv/xls")).build();
        }

      } catch (IOException e) {
        UploadController.log.error("Error sending to consumer: {}", e.getMessage());
      }

    }

    return Response.ok(new BaseResponse(200, "Upload success")).build();

  }

  private String getFileName(MultivaluedMap<String, String> header) {

    String[] contentDisposition = header.getFirst("Content-Disposition").split(";");

    for (String filename : contentDisposition) {
      if ((filename.trim().startsWith("filename"))) {

        String[] name = filename.split("=");

        return name[1].trim().replace("\"", "");
      }
    }
    return "unknown";
  }

}
